# UPchieve Income by Zip Code Calculator

## Prerequisites
You will need to have the SQLite command-line tool installed on your machine. If you have a Mac, you can use Homebrew with the following command: 
```
$ brew install sqlite
```

## Steps
**Note**: These are the steps performed most recently (in May 2023), however the underlying data is always subject to change in the future.
You can perform sanity checks along the way in addition to any explicitly called out in the steps below to unsure the updates are still necessary/accurate.

1. Download the income and zip code data (see [Data Sources](#Data Sources) below).
    * Unzip any files if necessary, and move the relevant CSV files into the `inputs` directory in the `zipcode-income` repository.
2. Open the command-line SQLite program from the root `zipcode-income` repository.
    ```
    $ cd /<path to repo>/zipcode-income
    $ sqlite3 zipcode_income_data_<today's date>.db
    ```
3. Set your output mode to CSV. 
    ```
    sqlite> .mode csv
    ```
4. Import the CSV files into the database.
    ```
    sqlite> .import inputs/ACSST5Y2021.S1901-Data.csv income_data // note: this name will change depending on the year.
    sqlite> .import inputs/zip_code_database.csv zipcode_data
    sqlite> .import inputs/qualifying_income_for_zip.csv qualifying_income_data
    ``` 
5. As a sanity check, make sure the tables exist and the counts appear accurate with the following commands:
    ```
    sqlite> .tables
    sqlite> SELECT COUNT(*) FROM income_data; // for reference, most recent data returned 33,775.
    sqlite> SELECT COUNT(*) FROM zipcode_data; // for reference, most recent data returned 42,735.
    ```
6. (Optional) Rename columns on income_data to make them easier to work with.
    * `NAME` actually refers to zip code, `S1901_C01_001E` is the total number of households, and `S1901_C02_012E` is the median income.
    * **Note**: If you don't complete this step, you'll have to remember to substitute `NAME`, `S1901_C01_001E`, `S1901_C02_012E` for `zip`, `total_households`, and `median_income`, respectively, in future steps.
    ```
    sqlite> ALTER TABLE income_data RENAME COLUMN NAME TO zip;
    sqlite> ALTER TABLE income_data RENAME COLUMN S1901_C01_001E TO total_households; 
    sqlite> ALTER TABLE income_data RENAME COLUMN S1901_C02_012E TO median_income; 
    ```
7. (Optional) Create indices on the columns we use to JOIN the two tables:
    ```
    sqlite> CREATE INDEX zip ON zipcode_data(zip);
    sqlite> CREATE INDEX income_zip ON income_data(zip);
    ```
8. Normalize data in the `income_data` table.
    * Remove the subheader row (row 2 in the dataset).
      ```
      sqlite> DELETE FROM income_data WHERE GEO_ID = 'Geography';
      ```
    * Remove `ZCTA5 ` from the beginning of all the zip codes.
      ```
      sqlite> UPDATE income_data SET zip = REPLACE(zip, 'ZCTA5 ', ''); 
      ```
    * Fix the values in the median income column.
      ```
      sqlite> UPDATE income_data SET median_income = NULL WHERE median_income = '-';
      sqlite> UPDATE income_data SET median_income = 2500 WHERE median_income = '2,500-';
      sqlite> UPDATE income_data SET median_income = 250000 WHERE median_income = '250,000+';
      ```
9. Perform other sanity checks. Some examples:
    * `type` column in `zipcode_data` table has values that are uppercased. Previously, it had been titlecased (e.g. MILITARY vs. Military).
    * verify there aren't other median income values in the `income_data` table with commas or non-digit characters.
10. Output the CSV.
    ```
    sqlite> .headers on
    sqlite> .output aggregated_data.csv
    sqlite> SELECT zipcode_data.zip, zipcode_data.state, 
            CASE 
                WHEN zipcode_data.state IN ('AS', 'FM', 'GU', 'MH', 'MP', 'PR', 'PW', 'VI') THEN IFNULL(income_data.median_income, 0)
                WHEN zipcode_data.type IN ('MILITARY', 'PO BOX', 'UNIQUE') THEN 1000000
                WHEN income_data.zip IS NULL THEN 1000000 -- Note: We could be smarter here.
                WHEN income_data.median_income IS NULL AND income_data.total_households <= 3000 THEN 0
                ELSE IFNULL(income_data.median_income, 1000000) 
            END AS median_family_income, 
            latitude, longitude, qualifying_income 
            FROM zipcode_data 
            LEFT JOIN income_data USING(zip) 
            LEFT JOIN qualifying_income USING(zip) 
            WHERE decommissioned = 0;
    sqlite> .output stdout
    ```
11. Copy `aggregated_data.csv` output file into `subway` and run the job `UpsertZipCodes` once deployed.
12. Zip the inputs, output, and database with the current date suffixed.

## Data Sources
We join three data sources: 
* income data from the most recent census data
* zipcodes from an organization called United States Zip Codes
* a list of qualifying incomes for zip codes in high cost of living areas. You will need to get this data from a teammate.

Note that there are likely more rows in the zip code data than the income data. 
If there is no income data available for a particular zip code, we give a default of 1,000,000.

### Income Data
Download the most recent census [income data](https://data.census.gov/cedsci/table?q=median%20income&g=0100000US%248600000&tid=ACSST5Y2019.S1901) as a CSV.

This downloads a zipped directory containing general notes, column metadata, and the actual data.

Columns we are interested in:
* `NAME`: zip code, with a prefix of `ZCTA5 ` (note the space is intentional), which is removed from the column in one of the normalization steps above.
* `S1901_C01_001E`: estimate of the total households. If you look at the column metadata, this column's description is `Estimate!!Households!!Total`.
* `S1901_C02_012E`: median income. If you look at the column metadata, this column's description is `Estimate!!Families!!Median income (dollars)`. 
  * Note: Not all of the values are numbers - for example, if the census has not provided income data for a zip, the value is `-` (instead of `NULL`). We normalize the values in this column in the steps above.

There is also a subheader row (i.e. the second row in the CSV) which is removed in one of the normalization steps above.

### Zip Code Data
Download the [zip code database](https://www.unitedstateszipcodes.org/zip-code-database/) as a CSV.

Columns we are interested in:
* zip
* state
* latitude
* longitude
* type: one-of PO BOX, MILITARY, UNIQUE, and STANDARD.

No normalization is required for this data.

### Qualifying Income Data
You will need to get this CSV from a teammate.

Columns:
* zip
* qualifying_income

Note: The file name and column names might be different - you can either update them to match the names in the steps above, or just update the steps to the names in the data you have.
